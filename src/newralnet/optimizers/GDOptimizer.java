package newralnet.optimizers;

import newralnet.core.Optimizer;
import newralnet.core.TrainableLayer;
import newralnet.util.LayerGradient;

/**
 * Gradient descent optimizer
 */
public class GDOptimizer extends Optimizer{
	double learningRate;
	
	/**
	 * Creates a new {@link GDOptimizer}
	 * @param learningRate - the learning rate (also known as step size ) which the optimizer should use
	 */
	public GDOptimizer(double learningRate) {
		this.learningRate=learningRate;
	}

	@Override
	public void optimize(TrainableLayer[] layers, LayerGradient[] gradients) {
		for(int l=0; l<layers.length; l++){
			//Bias adjustment step
			layers[l].biases=layers[l].biases.subtract(gradients[l].nablaBiases.mapMultiply(learningRate));
			//Weight adjustment step
			layers[l].weights=layers[l].weights.subtract(gradients[l].nablaWeights.scalarMultiply(learningRate));
		}
	}

	@Override
	public Optimizer copy() {
		return new GDOptimizer(learningRate);
	}
	
}
