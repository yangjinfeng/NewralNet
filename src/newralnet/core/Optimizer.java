package newralnet.core;

import newralnet.net.ModularNet;
import newralnet.util.LayerGradient;

/**
 *	An abstract class describing an {@link Optimizer} which is able to update a {@link ModularNet} given its {@link TrainableLayer}s and their {@link LayerGradient}s with respect to the {@link Cost} function.
 */
public abstract class Optimizer {
	/**
	 * Static double from interval [0,1) describing the amount of L2 Decay
	 */
	public static double L2DECAY=0.00;
	
	/**
	 * Updates the parameters of the {@link TrainableLayer}s with respect to the {@link LayerGradient}s
	 * @param layers - Array containing the {@link TrainableLayer}s which are about to get updated 
	 * @param gradients - Array (with the same length as the <code>layers</code> array) containing the {@link LayerGradient}s of the {@link TrainableLayer}s that should get updated
	 */
	public abstract void optimize(TrainableLayer[] layers, LayerGradient[] gradients);
	
	/**
	 * Performs a L2 Decay Update on the {@link TrainableLayer}s
	 * @param layers - The {@link TrainableLayer}s which should decay
	 */
	public void decay(TrainableLayer[] layers){//Regularization decay
		if(L2DECAY!=0)for(TrainableLayer tl:layers)tl.weights=tl.weights.scalarMultiply(1-L2DECAY);
	}
	
	/**
	 * @return A deep copy of the {@link Optimizer}
	 */
	public abstract Optimizer copy();
}
