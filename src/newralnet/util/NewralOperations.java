package newralnet.util;

/**
 * Class defining and containing the {@link NewralOperations} to be used with <code>NewralUtils.componentwiseOperation</code>
 */
public abstract class NewralOperations {
	
	/**
	 * Defines the action which the operation performs on corresponding entries of the input structures
	 * @param arguments - an array containing all corresponding entries of the given structures
	 * @return the result of the operation
	 */
	public abstract double operate(double[] arguments);
	
	public static HadamardProductOperation hadamardProductOperation=new HadamardProductOperation();
	public static HadamardDivisionOperation hadamardDivisionOperation=new HadamardDivisionOperation();
	public static LnOperation LnOperation=new LnOperation();
	public static AbsoluteOperation absoluteOperation=new AbsoluteOperation();
	public static SquareOperation squareOperation=new SquareOperation(); 
	public static ExpOperation expOperation=new ExpOperation(); 
}

/**
 * Multiplies all corresponding entries
 */
class HadamardProductOperation extends NewralOperations{

	@Override
	public double operate(double[] arguments) {
		double out=1;
		for(double d:arguments)out*=d;
		return out;
	}
	
}

/**
 * Divides the entries of the first structure by the entries of the second structure 
 */
class HadamardDivisionOperation extends NewralOperations{

	@Override
	public double operate(double[] arguments) {
		return arguments[0]/arguments[1];
	}
	
}

/**
 * Returns the <code>Math.log</code> of all entries 
 */
class LnOperation extends NewralOperations{

	@Override
	public double operate(double[] arguments) {
		return Math.log(arguments[0]);
	}
	
}

/**
 * Returns the <code>Math.exp</code> of all entries 
 */
class ExpOperation extends NewralOperations{

	@Override
	public double operate(double[] arguments) {
		return Math.exp(arguments[0]);
	}
	
}

/**
 * Returns the <code>Math.abs</code> of all entries 
 */
class AbsoluteOperation extends NewralOperations{

	@Override
	public double operate(double[] arguments) {
		return Math.abs(arguments[0]);
	}
	
}

/**
 * Returns the <code>Math.pow(entry, 2)</code> of all entries 
 */
class SquareOperation extends NewralOperations{

	@Override
	public double operate(double[] arguments) {
		return Math.pow(arguments[0], 2);
	}
	
}