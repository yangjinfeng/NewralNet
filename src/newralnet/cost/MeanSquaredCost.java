package newralnet.cost;

import org.apache.commons.math3.linear.RealMatrix;

import newralnet.core.Cost;
import newralnet.util.NewralUtils;

/**
 * Mean Squared {@link Cost} function
 */
public class MeanSquaredCost extends Cost{

	@Override
	public double cost(RealMatrix predicted, RealMatrix label) {
		return 0.5*NewralUtils.elementSum(NewralUtils.elementPow(predicted.subtract(label),2))/(predicted.getColumnDimension()*predicted.getColumnDimension());
	}

	@Override
	public RealMatrix costPrime(RealMatrix predicted, RealMatrix label) {
		return predicted.subtract(label);
	}
	
}
